import Card from "./Card";

const food = [
    {
        name: "Croquetas",
        desc: "asdadsad",
        photo: "../img/food/croquetas.jpg",
        idPhoto: "1",
        icons: "../img/icons/vegan.jpg",
    },
    {
        name: "Pechuga",
        desc: "asdfsadfasf",
        photo: "../img/food/pechugaVR.jpg",
        idPhoto: "2",
        icons: "../img/icons/spicy.jpg",
    },
];

function Food() {
    return(
        <div class="card__side card__side--front">                            
            <div class="card__picture card__picture--{food.idPhoto}">
                <div class="foodinfo">
                    <img src={food.photo} id="vegan" />
                </div>
                &nbsp;
            </div>
            <h4 class="card__heading">
                <span class="card__heading-span card__heading-span--1">
                {food.name}
                </span>
            </h4>
            <div class="card__details">
            <ul>                                    
                {food.desc}
            </ul>
            </div>
        </div>
        );
}

export default Food;
