# Proyecto-diseño: La Comuna

Vamos a realizar un proyecto web para un restaurante.
de momento vamos a realizar una página princpal, donde se podra visualizar algunas caracteristicas del sitio, comidas y otras opciones.
### Objetivos actuales (cada desarrollador pondrá la tarea que está llevando a cabo).
- Daniel: creando una animación fadeOut.
### Ideas:
- carta giratoria con plato, cuando gira muestra una breve descripcion
- iconos informacion nutricional. (vegano, sin gluten ...)
- menu del dia , boton, entre semana, y finde.
- sección para reservas
- sección "Hazte afiliado"
- carrusel de imagenes
- Propiedades de las imágenes de los platos: 
        Al posar el cursor la imagen se difumina y se muestra un texto por encima informando sobre los ingredientes.
- zona reseñas
- google maps

    
    https://www.figma.com/

### Estructura de carpetas:
    index.html : documento principal
    img: carpeta con imagenes 
    style: carpeta con los estilos css

    sass: carpeta donde se encuentran todos los componentes sass
        abstracts  
            _functions.scss-----> contiene las funciones de sass
            _mixins.scss--------> fragmentos de código sass reutilizable (con @include los incluimos dentro de las clases)
            _variables.scss-----> variables como colores, distintas dimensiones...
        base
            _animations.scss----> animaciones
            _base.scss----------> propiedades que van a compartir todas nuestras páginas.
            _typography.scss----> estilos que afecten a la apariencia de las fuentes.
            _utilities.scss-----> como los mixins pero con clases; para distintos formatos de márgenes, padding... 
        
        components-------------->  estilos de las unidades mínimas funcionales de html: botones, desplegables, pop-ups, divs animados...

        layaout
            _grid.scss----------> clases que recolocan los elementos internos de los divs que forman parte de ellas
            _header.scss--------> estilos de los componentes del header
            _footer.scss--------> estilos de los componentes del  footer
            _navigation.scss----> estilos de la barra de navegación y sus elementos
        pages-------------------->hojas de estilo de las distíntas páginas de nuestra web (estilos de sus secciones)
            _home.scss ---------> estilos de las secciones de la página home
        main.scss ---------------> archivo principal donde se importan todos los scss de las carpetas anteriores y el que importaremos al index

# Forma de trabajar con git:
- Hacemos un pull de la rama **main** del repositorio a nuestro repositorio local.
- Actualizamos nuestro node_modules utilizando el comando _yarn_ (a secas, es la abreviación de _yarn install_) con el repositorio local abierto desde el terminal(solo hace falta que lo hagamos al menos una vez).
- Al terminar de trabajar en tu repositorio local hacemos _git add_ y _git commit_.
- **Ántes de hacer un push a la rama main remota**:
    - Hacemos _git pull_ para comprobar primero desde el local si va a haber conflictos con la rama main (ej.Es probable que alguien haya hecho un push en el que ha escrito código sobre las mismas lineas en las que has añadido tú el tuyo).
    - **Si hay algún conflicto**, el terminal nos lo informa. Nos vamos al vscode y vamos resolviendo los conflictos desde ahí (el propio vscode te los resalta). Si el conflicto era que los 2 habéis cambiado la misma linea de código pero no tiene nada que ver la una con la otra, símplemente lo editamos dejando las 2. Si el conflicto trata de actualizar un mismo código, pues nos da la opción también de elegir cuál de los 2 dejar.
    - Guardamos los cambios (Git automáticamente reconoce que hemos resuelto el conflicto desde vscode al guardar el archivo).
    - Tras resolver el conflicto, hacemos de nuevo _git add_, _commit_ y por último hacemos el **push** .

**CUIDADO**-> Todos vamos a toquetear el index.html, cuidado cuando se resuelvan los conflictos ahí.

### Historial:

25/10/2021
    Creacion de estructuras de carpetas, desarrollo del diseño y creacion del index html.
